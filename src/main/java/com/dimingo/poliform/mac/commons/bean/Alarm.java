package com.dimingo.poliform.mac.commons.bean;

/*
 * Bean for keeping the values related to an alarm packet
 * coming from the mac device
 *
 */

import java.sql.Timestamp;


public class Alarm {

    /**
     * Instance variables
     */

    private long dataId;

    private byte slaveId;

    private Timestamp timestamp;

    private boolean green_01;
    private boolean green_02;
    private boolean green_03;
    private boolean green_04;
    private boolean green_05;
    private boolean green_06;
    private boolean green_07;
    private boolean green_08;
    private boolean green_09;
    private boolean green_10;
    private boolean green_11;
    private boolean green_12;
    private boolean green_13;
    private boolean green_14;
    private boolean green_15;
    private boolean green_16;
    private boolean green_17;
    private boolean green_18;
    private boolean green_19;
    private boolean green_20;
    private boolean green_21;
    private boolean green_22;
    private boolean green_23;
    private boolean green_24;
    private boolean green_25;
    private boolean green_26;
    private boolean green_27;
    private boolean green_28;
    private boolean green_29;
    private boolean green_30;

    private boolean red_01;
    private boolean red_02;
    private boolean red_03;
    private boolean red_04;
    private boolean red_05;
    private boolean red_06;
    private boolean red_07;
    private boolean red_08;
    private boolean red_09;
    private boolean red_10;
    private boolean red_11;
    private boolean red_12;
    private boolean red_13;
    private boolean red_14;
    private boolean red_15;
    private boolean red_16;
    private boolean red_17;
    private boolean red_18;
    private boolean red_19;
    private boolean red_20;
    private boolean red_21;
    private boolean red_22;
    private boolean red_23;
    private boolean red_24;
    private boolean red_25;
    private boolean red_26;
    private boolean red_27;
    private boolean red_28;
    private boolean red_29;
    private boolean red_30;

    private boolean anom_01;
    private boolean anom_02;
    private boolean anom_03;
    private boolean anom_04;

    private boolean reset;

    private boolean mute;


    /**
     * Default constructor
     */
    public Alarm() {

    } // end constructor


    /**
     * Getters and Setters
     */

    public long getDataId() {
        return dataId;
    }


    public void setDataId(long dataId) {
        this.dataId = dataId;
    }


    public byte getSlaveId() {
        return slaveId;
    }


    public void setSlaveId(byte slaveId) {
        this.slaveId = slaveId;
    }


    public Timestamp getTimestamp() {
        return timestamp;
    }


    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }


    public boolean isGreen_01() {
        return green_01;
    }


    public void setGreen_01(boolean green_01) {
        this.green_01 = green_01;
    }


    public boolean isGreen_02() {
        return green_02;
    }


    public void setGreen_02(boolean green_02) {
        this.green_02 = green_02;
    }


    public boolean isGreen_03() {
        return green_03;
    }


    public void setGreen_03(boolean green_03) {
        this.green_03 = green_03;
    }


    public boolean isGreen_04() {
        return green_04;
    }


    public void setGreen_04(boolean green_04) {
        this.green_04 = green_04;
    }


    public boolean isGreen_05() {
        return green_05;
    }


    public void setGreen_05(boolean green_05) {
        this.green_05 = green_05;
    }


    public boolean isGreen_06() {
        return green_06;
    }


    public void setGreen_06(boolean green_06) {
        this.green_06 = green_06;
    }


    public boolean isGreen_07() {
        return green_07;
    }


    public void setGreen_07(boolean green_07) {
        this.green_07 = green_07;
    }


    public boolean isGreen_08() {
        return green_08;
    }


    public void setGreen_08(boolean green_08) {
        this.green_08 = green_08;
    }


    public boolean isGreen_09() {
        return green_09;
    }


    public void setGreen_09(boolean green_09) {
        this.green_09 = green_09;
    }


    public boolean isGreen_10() {
        return green_10;
    }


    public void setGreen_10(boolean green_10) {
        this.green_10 = green_10;
    }


    public boolean isGreen_11() {
        return green_11;
    }


    public void setGreen_11(boolean green_11) {
        this.green_11 = green_11;
    }


    public boolean isGreen_12() {
        return green_12;
    }


    public void setGreen_12(boolean green_12) {
        this.green_12 = green_12;
    }


    public boolean isGreen_13() {
        return green_13;
    }


    public void setGreen_13(boolean green_13) {
        this.green_13 = green_13;
    }


    public boolean isGreen_14() {
        return green_14;
    }


    public void setGreen_14(boolean green_14) {
        this.green_14 = green_14;
    }


    public boolean isGreen_15() {
        return green_15;
    }


    public void setGreen_15(boolean green_15) {
        this.green_15 = green_15;
    }


    public boolean isGreen_16() {
        return green_16;
    }


    public void setGreen_16(boolean green_16) {
        this.green_16 = green_16;
    }


    public boolean isGreen_17() {
        return green_17;
    }


    public void setGreen_17(boolean green_17) {
        this.green_17 = green_17;
    }


    public boolean isGreen_18() {
        return green_18;
    }


    public void setGreen_18(boolean green_18) {
        this.green_18 = green_18;
    }


    public boolean isGreen_19() {
        return green_19;
    }


    public void setGreen_19(boolean green_19) {
        this.green_19 = green_19;
    }


    public boolean isGreen_20() {
        return green_20;
    }


    public void setGreen_20(boolean green_20) {
        this.green_20 = green_20;
    }


    public boolean isGreen_21() {
        return green_21;
    }


    public void setGreen_21(boolean green_21) {
        this.green_21 = green_21;
    }


    public boolean isGreen_22() {
        return green_22;
    }


    public void setGreen_22(boolean green_22) {
        this.green_22 = green_22;
    }


    public boolean isGreen_23() {
        return green_23;
    }


    public void setGreen_23(boolean green_23) {
        this.green_23 = green_23;
    }


    public boolean isGreen_24() {
        return green_24;
    }


    public void setGreen_24(boolean green_24) {
        this.green_24 = green_24;
    }


    public boolean isGreen_25() {
        return green_25;
    }


    public void setGreen_25(boolean green_25) {
        this.green_25 = green_25;
    }


    public boolean isGreen_26() {
        return green_26;
    }


    public void setGreen_26(boolean green_26) {
        this.green_26 = green_26;
    }


    public boolean isGreen_27() {
        return green_27;
    }


    public void setGreen_27(boolean green_27) {
        this.green_27 = green_27;
    }


    public boolean isGreen_28() {
        return green_28;
    }


    public void setGreen_28(boolean green_28) {
        this.green_28 = green_28;
    }


    public boolean isGreen_29() {
        return green_29;
    }


    public void setGreen_29(boolean green_29) {
        this.green_29 = green_29;
    }


    public boolean isGreen_30() {
        return green_30;
    }


    public void setGreen_30(boolean green_30) {
        this.green_30 = green_30;
    }


    public boolean isRed_01() {
        return red_01;
    }


    public void setRed_01(boolean red_01) {
        this.red_01 = red_01;
    }


    public boolean isRed_02() {
        return red_02;
    }


    public void setRed_02(boolean red_02) {
        this.red_02 = red_02;
    }


    public boolean isRed_03() {
        return red_03;
    }


    public void setRed_03(boolean red_03) {
        this.red_03 = red_03;
    }


    public boolean isRed_04() {
        return red_04;
    }


    public void setRed_04(boolean red_04) {
        this.red_04 = red_04;
    }


    public boolean isRed_05() {
        return red_05;
    }


    public void setRed_05(boolean red_05) {
        this.red_05 = red_05;
    }


    public boolean isRed_06() {
        return red_06;
    }


    public void setRed_06(boolean red_06) {
        this.red_06 = red_06;
    }


    public boolean isRed_07() {
        return red_07;
    }


    public void setRed_07(boolean red_07) {
        this.red_07 = red_07;
    }


    public boolean isRed_08() {
        return red_08;
    }


    public void setRed_08(boolean red_08) {
        this.red_08 = red_08;
    }


    public boolean isRed_09() {
        return red_09;
    }


    public void setRed_09(boolean red_09) {
        this.red_09 = red_09;
    }


    public boolean isRed_10() {
        return red_10;
    }


    public void setRed_10(boolean red_10) {
        this.red_10 = red_10;
    }


    public boolean isRed_11() {
        return red_11;
    }


    public void setRed_11(boolean red_11) {
        this.red_11 = red_11;
    }


    public boolean isRed_12() {
        return red_12;
    }


    public void setRed_12(boolean red_12) {
        this.red_12 = red_12;
    }


    public boolean isRed_13() {
        return red_13;
    }


    public void setRed_13(boolean red_13) {
        this.red_13 = red_13;
    }


    public boolean isRed_14() {
        return red_14;
    }


    public void setRed_14(boolean red_14) {
        this.red_14 = red_14;
    }


    public boolean isRed_15() {
        return red_15;
    }


    public void setRed_15(boolean red_15) {
        this.red_15 = red_15;
    }


    public boolean isRed_16() {
        return red_16;
    }


    public void setRed_16(boolean red_16) {
        this.red_16 = red_16;
    }


    public boolean isRed_17() {
        return red_17;
    }


    public void setRed_17(boolean red_17) {
        this.red_17 = red_17;
    }


    public boolean isRed_18() {
        return red_18;
    }


    public void setRed_18(boolean red_18) {
        this.red_18 = red_18;
    }


    public boolean isRed_19() {
        return red_19;
    }


    public void setRed_19(boolean red_19) {
        this.red_19 = red_19;
    }


    public boolean isRed_20() {
        return red_20;
    }


    public void setRed_20(boolean red_20) {
        this.red_20 = red_20;
    }


    public boolean isRed_21() {
        return red_21;
    }


    public void setRed_21(boolean red_21) {
        this.red_21 = red_21;
    }


    public boolean isRed_22() {
        return red_22;
    }


    public void setRed_22(boolean red_22) {
        this.red_22 = red_22;
    }


    public boolean isRed_23() {
        return red_23;
    }


    public void setRed_23(boolean red_23) {
        this.red_23 = red_23;
    }


    public boolean isRed_24() {
        return red_24;
    }


    public void setRed_24(boolean red_24) {
        this.red_24 = red_24;
    }


    public boolean isRed_25() {
        return red_25;
    }


    public void setRed_25(boolean red_25) {
        this.red_25 = red_25;
    }


    public boolean isRed_26() {
        return red_26;
    }


    public void setRed_26(boolean red_26) {
        this.red_26 = red_26;
    }


    public boolean isRed_27() {
        return red_27;
    }


    public void setRed_27(boolean red_27) {
        this.red_27 = red_27;
    }


    public boolean isRed_28() {
        return red_28;
    }


    public void setRed_28(boolean red_28) {
        this.red_28 = red_28;
    }


    public boolean isRed_29() {
        return red_29;
    }


    public void setRed_29(boolean red_29) {
        this.red_29 = red_29;
    }


    public boolean isRed_30() {
        return red_30;
    }


    public void setRed_30(boolean red_30) {
        this.red_30 = red_30;
    }


    public boolean isAnom_01() {
        return anom_01;
    }


    public void setAnom_01(boolean anom_01) {
        this.anom_01 = anom_01;
    }


    public boolean isAnom_02() {
        return anom_02;
    }


    public void setAnom_02(boolean anom_02) {
        this.anom_02 = anom_02;
    }


    public boolean isAnom_03() {
        return anom_03;
    }


    public void setAnom_03(boolean anom_03) {
        this.anom_03 = anom_03;
    }


    public boolean isAnom_04() {
        return anom_04;
    }


    public void setAnom_04(boolean anom_04) {
        this.anom_04 = anom_04;
    }


    public boolean isReset() {
        return reset;
    }


    public void setReset(boolean reset) {
        this.reset = reset;
    }


    public boolean isMute() {
        return mute;
    }


    public void setMute(boolean mute) {
        this.mute = mute;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + (anom_01 ? 1231 : 1237);
        result = prime * result + (anom_02 ? 1231 : 1237);
        result = prime * result + (anom_03 ? 1231 : 1237);
        result = prime * result + (anom_04 ? 1231 : 1237);
        result = prime * result + (green_01 ? 1231 : 1237);
        result = prime * result + (green_02 ? 1231 : 1237);
        result = prime * result + (green_03 ? 1231 : 1237);
        result = prime * result + (green_04 ? 1231 : 1237);
        result = prime * result + (green_05 ? 1231 : 1237);
        result = prime * result + (green_06 ? 1231 : 1237);
        result = prime * result + (green_07 ? 1231 : 1237);
        result = prime * result + (green_08 ? 1231 : 1237);
        result = prime * result + (green_09 ? 1231 : 1237);
        result = prime * result + (green_10 ? 1231 : 1237);
        result = prime * result + (green_11 ? 1231 : 1237);
        result = prime * result + (green_12 ? 1231 : 1237);
        result = prime * result + (green_13 ? 1231 : 1237);
        result = prime * result + (green_14 ? 1231 : 1237);
        result = prime * result + (green_15 ? 1231 : 1237);
        result = prime * result + (green_16 ? 1231 : 1237);
        result = prime * result + (green_17 ? 1231 : 1237);
        result = prime * result + (green_18 ? 1231 : 1237);
        result = prime * result + (green_19 ? 1231 : 1237);
        result = prime * result + (green_20 ? 1231 : 1237);
        result = prime * result + (green_21 ? 1231 : 1237);
        result = prime * result + (green_22 ? 1231 : 1237);
        result = prime * result + (green_23 ? 1231 : 1237);
        result = prime * result + (green_24 ? 1231 : 1237);
        result = prime * result + (green_25 ? 1231 : 1237);
        result = prime * result + (green_26 ? 1231 : 1237);
        result = prime * result + (green_27 ? 1231 : 1237);
        result = prime * result + (green_28 ? 1231 : 1237);
        result = prime * result + (green_29 ? 1231 : 1237);
        result = prime * result + (green_30 ? 1231 : 1237);
        result = prime * result + (mute ? 1231 : 1237);
        result = prime * result + (red_01 ? 1231 : 1237);
        result = prime * result + (red_02 ? 1231 : 1237);
        result = prime * result + (red_03 ? 1231 : 1237);
        result = prime * result + (red_04 ? 1231 : 1237);
        result = prime * result + (red_05 ? 1231 : 1237);
        result = prime * result + (red_06 ? 1231 : 1237);
        result = prime * result + (red_07 ? 1231 : 1237);
        result = prime * result + (red_08 ? 1231 : 1237);
        result = prime * result + (red_09 ? 1231 : 1237);
        result = prime * result + (red_10 ? 1231 : 1237);
        result = prime * result + (red_11 ? 1231 : 1237);
        result = prime * result + (red_12 ? 1231 : 1237);
        result = prime * result + (red_13 ? 1231 : 1237);
        result = prime * result + (red_14 ? 1231 : 1237);
        result = prime * result + (red_15 ? 1231 : 1237);
        result = prime * result + (red_16 ? 1231 : 1237);
        result = prime * result + (red_17 ? 1231 : 1237);
        result = prime * result + (red_18 ? 1231 : 1237);
        result = prime * result + (red_19 ? 1231 : 1237);
        result = prime * result + (red_20 ? 1231 : 1237);
        result = prime * result + (red_21 ? 1231 : 1237);
        result = prime * result + (red_22 ? 1231 : 1237);
        result = prime * result + (red_23 ? 1231 : 1237);
        result = prime * result + (red_24 ? 1231 : 1237);
        result = prime * result + (red_25 ? 1231 : 1237);
        result = prime * result + (red_26 ? 1231 : 1237);
        result = prime * result + (red_27 ? 1231 : 1237);
        result = prime * result + (red_28 ? 1231 : 1237);
        result = prime * result + (red_29 ? 1231 : 1237);
        result = prime * result + (red_30 ? 1231 : 1237);
        result = prime * result + (reset ? 1231 : 1237);
        result = prime * result + slaveId;
        return result;

    } // end hashCode


    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Alarm))
            return false;

        Alarm other = (Alarm) obj;
        if (anom_01 != other.anom_01)
            return false;
        if (anom_02 != other.anom_02)
            return false;
        if (anom_03 != other.anom_03)
            return false;
        if (anom_04 != other.anom_04)
            return false;
        if (green_01 != other.green_01)
            return false;
        if (green_02 != other.green_02)
            return false;
        if (green_03 != other.green_03)
            return false;
        if (green_04 != other.green_04)
            return false;
        if (green_05 != other.green_05)
            return false;
        if (green_06 != other.green_06)
            return false;
        if (green_07 != other.green_07)
            return false;
        if (green_08 != other.green_08)
            return false;
        if (green_09 != other.green_09)
            return false;
        if (green_10 != other.green_10)
            return false;
        if (green_11 != other.green_11)
            return false;
        if (green_12 != other.green_12)
            return false;
        if (green_13 != other.green_13)
            return false;
        if (green_14 != other.green_14)
            return false;
        if (green_15 != other.green_15)
            return false;
        if (green_16 != other.green_16)
            return false;
        if (green_17 != other.green_17)
            return false;
        if (green_18 != other.green_18)
            return false;
        if (green_19 != other.green_19)
            return false;
        if (green_20 != other.green_20)
            return false;
        if (green_21 != other.green_21)
            return false;
        if (green_22 != other.green_22)
            return false;
        if (green_23 != other.green_23)
            return false;
        if (green_24 != other.green_24)
            return false;
        if (green_25 != other.green_25)
            return false;
        if (green_26 != other.green_26)
            return false;
        if (green_27 != other.green_27)
            return false;
        if (green_28 != other.green_28)
            return false;
        if (green_29 != other.green_29)
            return false;
        if (green_30 != other.green_30)
            return false;
        if (mute != other.mute)
            return false;
        if (red_01 != other.red_01)
            return false;
        if (red_02 != other.red_02)
            return false;
        if (red_03 != other.red_03)
            return false;
        if (red_04 != other.red_04)
            return false;
        if (red_05 != other.red_05)
            return false;
        if (red_06 != other.red_06)
            return false;
        if (red_07 != other.red_07)
            return false;
        if (red_08 != other.red_08)
            return false;
        if (red_09 != other.red_09)
            return false;
        if (red_10 != other.red_10)
            return false;
        if (red_11 != other.red_11)
            return false;
        if (red_12 != other.red_12)
            return false;
        if (red_13 != other.red_13)
            return false;
        if (red_14 != other.red_14)
            return false;
        if (red_15 != other.red_15)
            return false;
        if (red_16 != other.red_16)
            return false;
        if (red_17 != other.red_17)
            return false;
        if (red_18 != other.red_18)
            return false;
        if (red_19 != other.red_19)
            return false;
        if (red_20 != other.red_20)
            return false;
        if (red_21 != other.red_21)
            return false;
        if (red_22 != other.red_22)
            return false;
        if (red_23 != other.red_23)
            return false;
        if (red_24 != other.red_24)
            return false;
        if (red_25 != other.red_25)
            return false;
        if (red_26 != other.red_26)
            return false;
        if (red_27 != other.red_27)
            return false;
        if (red_28 != other.red_28)
            return false;
        if (red_29 != other.red_29)
            return false;
        if (red_30 != other.red_30)
            return false;
        if (reset != other.reset)
            return false;
        if (slaveId != other.slaveId)
            return false;
        return true;

    } // end equals

    @Override
    public String toString() {
        return "Alarm{" +
                "dataId=" + dataId +
                ", slaveId=" + slaveId +
                ", timestamp=" + timestamp +
                ", green_01=" + green_01 +
                ", green_02=" + green_02 +
                ", green_03=" + green_03 +
                ", green_04=" + green_04 +
                ", green_05=" + green_05 +
                ", green_06=" + green_06 +
                ", green_07=" + green_07 +
                ", green_08=" + green_08 +
                ", green_09=" + green_09 +
                ", green_10=" + green_10 +
                ", green_11=" + green_11 +
                ", green_12=" + green_12 +
                ", green_13=" + green_13 +
                ", green_14=" + green_14 +
                ", green_15=" + green_15 +
                ", green_16=" + green_16 +
                ", green_17=" + green_17 +
                ", green_18=" + green_18 +
                ", green_19=" + green_19 +
                ", green_20=" + green_20 +
                ", green_21=" + green_21 +
                ", green_22=" + green_22 +
                ", green_23=" + green_23 +
                ", green_24=" + green_24 +
                ", green_25=" + green_25 +
                ", green_26=" + green_26 +
                ", green_27=" + green_27 +
                ", green_28=" + green_28 +
                ", green_29=" + green_29 +
                ", green_30=" + green_30 +
                ", red_01=" + red_01 +
                ", red_02=" + red_02 +
                ", red_03=" + red_03 +
                ", red_04=" + red_04 +
                ", red_05=" + red_05 +
                ", red_06=" + red_06 +
                ", red_07=" + red_07 +
                ", red_08=" + red_08 +
                ", red_09=" + red_09 +
                ", red_10=" + red_10 +
                ", red_11=" + red_11 +
                ", red_12=" + red_12 +
                ", red_13=" + red_13 +
                ", red_14=" + red_14 +
                ", red_15=" + red_15 +
                ", red_16=" + red_16 +
                ", red_17=" + red_17 +
                ", red_18=" + red_18 +
                ", red_19=" + red_19 +
                ", red_20=" + red_20 +
                ", red_21=" + red_21 +
                ", red_22=" + red_22 +
                ", red_23=" + red_23 +
                ", red_24=" + red_24 +
                ", red_25=" + red_25 +
                ", red_26=" + red_26 +
                ", red_27=" + red_27 +
                ", red_28=" + red_28 +
                ", red_29=" + red_29 +
                ", red_30=" + red_30 +
                ", anom_01=" + anom_01 +
                ", anom_02=" + anom_02 +
                ", anom_03=" + anom_03 +
                ", anom_04=" + anom_04 +
                ", reset=" + reset +
                ", mute=" + mute +
                '}';
    }
} // end class
