package com.dimingo.poliform.mac.commons.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.dimingo.poliform.mac.commons.bean.Device;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.dimingo.poliform.mac.commons.bean.Alarm;
import com.dimingo.poliform.mac.commons.bean.AlarmDescription;

/**
 * AlarmDao
 * 
 * @author giovanni
 *
 */
public class AlarmDao implements IAlarmDao {

	private final static Logger logger = Logger.getLogger(AlarmDao.class);
	
	// SimpleJdbcTemplate-style...
	private SimpleJdbcTemplate simpleJdbcTemplate;

	public void setDataSource(DataSource dataSource) {
	    this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}
	
	
	/**
	 * Default constructor
	 */
	public AlarmDao() {
		
	} // end constructor

	
	public Alarm findLastAlarmBySlaveId(byte slaveId) {
		
		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);
		
		// define the SQL select statement
		String sql = "select * " +
					 "from alarms " +
					 "where data_id = ( " +
					 		"select max(data_id) " +
					 		"from alarms " +
					 		"where slave_id = ?" +  // hexadecimal string e.g. '01', '02', '1F', 'A0'
					 		")";
		
//		ParameterizedRowMapper<Alarm> mapper = new ParameterizedRowMapper<Alarm>() {
		RowMapper<Alarm> mapper = new RowMapper<Alarm>() {
		    
	        // implement the mapRow method in order to create an Alarm object
	        public Alarm mapRow(ResultSet rs, int rowNum) throws SQLException {
	            
	        	Alarm alarm = new Alarm();
	        	
	            alarm.setDataId(rs.getLong("DATA_ID"));
	            alarm.setSlaveId(Byte.parseByte(rs.getString("SLAVE_ID").trim()));
	            alarm.setTimestamp(rs.getTimestamp("RECEIVED_TIME"));
	            
	            alarm.setGreen_01(rs.getBoolean("GREEN_01"));
	            alarm.setGreen_02(rs.getBoolean("GREEN_02"));
	            alarm.setGreen_03(rs.getBoolean("GREEN_03"));
	            alarm.setGreen_04(rs.getBoolean("GREEN_04"));
	            alarm.setGreen_05(rs.getBoolean("GREEN_05"));
	            alarm.setGreen_06(rs.getBoolean("GREEN_06"));
	            alarm.setGreen_07(rs.getBoolean("GREEN_07"));
	            alarm.setGreen_08(rs.getBoolean("GREEN_08"));
	            alarm.setGreen_09(rs.getBoolean("GREEN_09"));
	            alarm.setGreen_10(rs.getBoolean("GREEN_10"));
	            alarm.setGreen_11(rs.getBoolean("GREEN_11"));
	            alarm.setGreen_12(rs.getBoolean("GREEN_12"));
	            alarm.setGreen_13(rs.getBoolean("GREEN_13"));
	            alarm.setGreen_14(rs.getBoolean("GREEN_14"));
	            alarm.setGreen_15(rs.getBoolean("GREEN_15"));
	            alarm.setGreen_16(rs.getBoolean("GREEN_16"));
	            alarm.setGreen_17(rs.getBoolean("GREEN_17"));
	            alarm.setGreen_18(rs.getBoolean("GREEN_18"));
	            alarm.setGreen_19(rs.getBoolean("GREEN_19"));
	            alarm.setGreen_20(rs.getBoolean("GREEN_20"));
	            alarm.setGreen_21(rs.getBoolean("GREEN_21"));
	            alarm.setGreen_22(rs.getBoolean("GREEN_22"));
	            alarm.setGreen_23(rs.getBoolean("GREEN_23"));
	            alarm.setGreen_24(rs.getBoolean("GREEN_24"));
	            alarm.setGreen_25(rs.getBoolean("GREEN_25"));
	            alarm.setGreen_26(rs.getBoolean("GREEN_26"));
	            alarm.setGreen_27(rs.getBoolean("GREEN_27"));
	            alarm.setGreen_28(rs.getBoolean("GREEN_28"));
	            alarm.setGreen_29(rs.getBoolean("GREEN_29"));
	            alarm.setGreen_30(rs.getBoolean("GREEN_30"));
	            
	            alarm.setRed_01(rs.getBoolean("RED_01"));
	            alarm.setRed_02(rs.getBoolean("RED_02"));
	            alarm.setRed_03(rs.getBoolean("RED_03"));
	            alarm.setRed_04(rs.getBoolean("RED_04"));
	            alarm.setRed_05(rs.getBoolean("RED_05"));
	            alarm.setRed_06(rs.getBoolean("RED_06"));
	            alarm.setRed_07(rs.getBoolean("RED_07"));
	            alarm.setRed_08(rs.getBoolean("RED_08"));
	            alarm.setRed_09(rs.getBoolean("RED_09"));
	            alarm.setRed_10(rs.getBoolean("RED_10"));
	            alarm.setRed_11(rs.getBoolean("RED_11"));
	            alarm.setRed_12(rs.getBoolean("RED_12"));
	            alarm.setRed_13(rs.getBoolean("RED_13"));
	            alarm.setRed_14(rs.getBoolean("RED_14"));
	            alarm.setRed_15(rs.getBoolean("RED_15"));
	            alarm.setRed_16(rs.getBoolean("RED_16"));
	            alarm.setRed_17(rs.getBoolean("RED_17"));
	            alarm.setRed_18(rs.getBoolean("RED_18"));
	            alarm.setRed_19(rs.getBoolean("RED_19"));
	            alarm.setRed_20(rs.getBoolean("RED_20"));
	            alarm.setRed_21(rs.getBoolean("RED_21"));
	            alarm.setRed_22(rs.getBoolean("RED_22"));
	            alarm.setRed_23(rs.getBoolean("RED_23"));
	            alarm.setRed_24(rs.getBoolean("RED_24"));
	            alarm.setRed_25(rs.getBoolean("RED_25"));
	            alarm.setRed_26(rs.getBoolean("RED_26"));
	            alarm.setRed_27(rs.getBoolean("RED_27"));
	            alarm.setRed_28(rs.getBoolean("RED_28"));
	            alarm.setRed_29(rs.getBoolean("RED_29"));
	            alarm.setRed_30(rs.getBoolean("RED_30"));
	            
	            alarm.setAnom_01(rs.getBoolean("ANOM_01"));
	            alarm.setAnom_02(rs.getBoolean("ANOM_02"));
	            alarm.setAnom_03(rs.getBoolean("ANOM_03"));
	            alarm.setAnom_04(rs.getBoolean("ANOM_04"));
	            
	            alarm.setReset(rs.getBoolean("RESET"));
	            alarm.setMute(rs.getBoolean("MUTE"));
	            
	            return alarm;
	        	
	        } // end mapRow
	        
	    }; // end anonymous inner class RowMapper
	    
	    Alarm alarm = null;

	    try {
	    	
	    	// retrieve the alarm from DB
			alarm = simpleJdbcTemplate.queryForObject(sql, mapper, slaveIdString);
			
		} catch (DataAccessException e) {

	    	logger.error("ERROR while retrieving the last alarm", e);
//			e.printStackTrace();
			
		} // end catch
	    
	    return alarm;
		
	} // end findLastAlarmBySlaveId

	
	public void storeAlarm(Alarm alarm) {
		
		// define the SQL insert statement
		String sql = "INSERT INTO ALARMS ( " +
				        "DATA_ID, " +
				        "SLAVE_ID, " +
				        "RECEIVED_TIME, " +
				        "GREEN_01, " +
				        "GREEN_02, " +
				        "GREEN_03, " +
				        "GREEN_04, " +
				        "GREEN_05, " +
				        "GREEN_06, " +
				        "GREEN_07, " +
				        "GREEN_08, " +
				        "GREEN_09, " +
				        "GREEN_10, " +
				        "GREEN_11, " +
				        "GREEN_12, " +
				        "GREEN_13, " +
				        "GREEN_14, " +
				        "GREEN_15, " +
				        "GREEN_16, " +
				        "GREEN_17, " +
				        "GREEN_18, " +
				        "GREEN_19, " +
				        "GREEN_20, " +
				        "GREEN_21, " +
				        "GREEN_22, " +
				        "GREEN_23, " +
				        "GREEN_24, " +
				        "GREEN_25, " +
				        "GREEN_26, " +
				        "GREEN_27, " +
				        "GREEN_28, " +
				        "GREEN_29, " +
				        "GREEN_30, " +
				        "RED_01, " +
				        "RED_02, " +
				        "RED_03, " +
				        "RED_04, " +
				        "RED_05, " +
				        "RED_06, " +
				        "RED_07, " +
				        "RED_08, " +
				        "RED_09, " +
				        "RED_10, " +
				        "RED_11, " +
				        "RED_12, " +
				        "RED_13, " +
				        "RED_14, " +
				        "RED_15, " +
				        "RED_16, " +
				        "RED_17, " +
				        "RED_18, " +
				        "RED_19, " +
				        "RED_20, " +
				        "RED_21, " +
				        "RED_22, " +
				        "RED_23, " +
				        "RED_24, " +
				        "RED_25, " +
				        "RED_26, " +
				        "RED_27, " +
				        "RED_28, " +
				        "RED_29, " +
				        "RED_30, " +
				        "ANOM_01, " +
				        "ANOM_02, " +
				        "ANOM_03, " +
				        "ANOM_04, " +
				        "RESET, " +
				        "MUTE " +
				    ") " +
				    "VALUES " +
				    "( " +
				        "DEFAULT, " +
				        "?, " +
				        "?, " +
				        "?, " +
				        "?, " +
				        "?, " +
				        "?, " +
				        "?, " +
				        "?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"?, " +
						"? " +
  				    ")"; // end sql string
		
		//call the update method for executing the insert into the db
		simpleJdbcTemplate.update(sql, 
				
				String.format("%1$02X", alarm.getSlaveId()),
				new Timestamp(new Date().getTime()),
				
		        alarm.isGreen_01() ? "1" : "0",
		       	alarm.isGreen_02() ? "1" : "0",
		       	alarm.isGreen_03() ? "1" : "0",
		       	alarm.isGreen_04() ? "1" : "0",
		       	alarm.isGreen_05() ? "1" : "0",
		       	alarm.isGreen_06() ? "1" : "0",
		       	alarm.isGreen_07() ? "1" : "0",
		       	alarm.isGreen_08() ? "1" : "0",
		       	alarm.isGreen_09() ? "1" : "0",
		       	alarm.isGreen_10() ? "1" : "0",
       			alarm.isGreen_11() ? "1" : "0",
		       	alarm.isGreen_12() ? "1" : "0",
		       	alarm.isGreen_13() ? "1" : "0",
		       	alarm.isGreen_14() ? "1" : "0",
		       	alarm.isGreen_15() ? "1" : "0",
		       	alarm.isGreen_16() ? "1" : "0",
		       	alarm.isGreen_17() ? "1" : "0",
		       	alarm.isGreen_18() ? "1" : "0",
		       	alarm.isGreen_19() ? "1" : "0",
		       	alarm.isGreen_20() ? "1" : "0",
       			alarm.isGreen_21() ? "1" : "0",
		       	alarm.isGreen_22() ? "1" : "0",
		       	alarm.isGreen_23() ? "1" : "0",
		       	alarm.isGreen_24() ? "1" : "0",
		       	alarm.isGreen_25() ? "1" : "0",
		       	alarm.isGreen_26() ? "1" : "0",
		       	alarm.isGreen_27() ? "1" : "0",
		       	alarm.isGreen_28() ? "1" : "0",
		       	alarm.isGreen_29() ? "1" : "0",
		       	alarm.isGreen_30() ? "1" : "0",
		       			
				alarm.isRed_01() ? "1" : "0",
		       	alarm.isRed_02() ? "1" : "0",
		       	alarm.isRed_03() ? "1" : "0",
		       	alarm.isRed_04() ? "1" : "0",
		       	alarm.isRed_05() ? "1" : "0",
		       	alarm.isRed_06() ? "1" : "0",
		       	alarm.isRed_07() ? "1" : "0",
		       	alarm.isRed_08() ? "1" : "0",
		       	alarm.isRed_09() ? "1" : "0",
		       	alarm.isRed_10() ? "1" : "0",
       			alarm.isRed_11() ? "1" : "0",
		       	alarm.isRed_12() ? "1" : "0",
		       	alarm.isRed_13() ? "1" : "0",
		       	alarm.isRed_14() ? "1" : "0",
		       	alarm.isRed_15() ? "1" : "0",
		       	alarm.isRed_16() ? "1" : "0",
		       	alarm.isRed_17() ? "1" : "0",
		       	alarm.isRed_18() ? "1" : "0",
		       	alarm.isRed_19() ? "1" : "0",
		       	alarm.isRed_20() ? "1" : "0",
       			alarm.isRed_21() ? "1" : "0",
		       	alarm.isRed_22() ? "1" : "0",
		       	alarm.isRed_23() ? "1" : "0",
		       	alarm.isRed_24() ? "1" : "0",
		       	alarm.isRed_25() ? "1" : "0",
		       	alarm.isRed_26() ? "1" : "0",
		       	alarm.isRed_27() ? "1" : "0",
		       	alarm.isRed_28() ? "1" : "0",
		       	alarm.isRed_29() ? "1" : "0",
		       	alarm.isRed_30() ? "1" : "0",
		       			
       			alarm.isAnom_01() ? "1" : "0",
		       	alarm.isAnom_02() ? "1" : "0",
		       	alarm.isAnom_03() ? "1" : "0",
		       	alarm.isAnom_04() ? "1" : "0",
	   
		       	alarm.isReset() ? "1" : "0",
		       	alarm.isMute() ? "1" : "0"

				); // end update call
		
	} // end storeAlarm
	
	
	public AlarmDescription findAlarmDescription(byte slaveId, byte alarmId) {
		
		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);
		
		// convert the alarmId byte into an hexadecimal string with two-chars, zero-filled
		String alarmIdString = String.format("%1$02X", alarmId);
		
		// define the SQL select statement
		String sql = "select * " +
					 "from alarm_descriptions " +
					 "where slave_id = ?" +  // hexadecimal string e.g. '01', '02', '1F', 'A0'
					 "  and alarm_id = ?";	 // hexadecimal string e.g. '01', '02', '1F', 'A0'
					 
		// define a row mapper
		RowMapper<AlarmDescription> mapper = new RowMapper<AlarmDescription>() {
		    
	        // implement the mapRow method in order to create an AlarmDescription object
	        public AlarmDescription mapRow(ResultSet rs, int rowNum) throws SQLException {
	            
	        	AlarmDescription alarmDescription = new AlarmDescription();
	        	
	        	alarmDescription.setSlaveId(Byte.parseByte(rs.getString("SLAVE_ID").trim()));
	        	alarmDescription.setAlarmId(Byte.parseByte(rs.getString("ALARM_ID").trim()));
	            alarmDescription.setAlarmDescription(rs.getString("ALARM_DESCRIPTION").trim());
	            
	            return alarmDescription;
	        	
	        } // end mapRow
	        
	    }; // end anonymous inner class RowMapper
	    
	    AlarmDescription alarmDescription = null;

	    try {
	    	
	    	// retrieve the alarm description from DB
	    	alarmDescription = simpleJdbcTemplate.queryForObject(sql, mapper, slaveIdString, alarmIdString);
			
		} catch (DataAccessException e) {

			logger.error("ERROR while retrieving the alarm description", e);
//			e.printStackTrace();
			
		} // end catch
	    
	    return alarmDescription;
		
	} // end findAlarmDescription


	/**
	 * 
	 */
	@Override
	public List<AlarmDescription> findAllAlarmDescriptions(byte slaveId) {
		
		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);
		
		// define the SQL select statement
		String sql = "select * " +
					 "from alarm_descriptions " +
					 "where slave_id = ? " +   // hexadecimal string e.g. '01', '02', '1F', 'A0'
					 "order by alarm_id";
					 
		// define a row mapper
		RowMapper<AlarmDescription> mapper = new RowMapper<AlarmDescription>() {
		    
	        // implement the mapRow method in order to create an AlarmDescription object
	        public AlarmDescription mapRow(ResultSet rs, int rowNum) throws SQLException {
	            
	        	AlarmDescription alarmDescription = new AlarmDescription();
	        	
	        	alarmDescription.setSlaveId(Byte.parseByte(rs.getString("SLAVE_ID").trim()));
	        	alarmDescription.setAlarmId(Byte.parseByte(rs.getString("ALARM_ID").trim()));
	            alarmDescription.setAlarmDescription(rs.getString("ALARM_DESCRIPTION").trim());
	            
	            return alarmDescription;
	        	
	        } // end mapRow
	        
	    }; // end anonymous inner class RowMapper
	    
	    List<AlarmDescription> alarmDescriptionList = null;

	    try {
	    	
	    	// retrieve the alarm description list from DB
	    	alarmDescriptionList = simpleJdbcTemplate.query(sql, mapper, slaveIdString);
			
		} catch (DataAccessException e) {

			logger.error("ERROR while retrieving the alarm description list", e);
//			e.printStackTrace();
			
		} // end catch
	    
	    return alarmDescriptionList;
		
	} // end findAllAlarmDescriptions


	/**
	 *
	 */
	@Override
	public void updateAlarmDescription(AlarmDescription alarmDescription) {

		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", alarmDescription.getSlaveId());

		// convert the alarmId into a string with two-chars, zero-filled
		String alarmIdString = String.format("%1$02d", alarmDescription.getAlarmId());

		// define the SQL update statement
		String sql = "update ALARM_DESCRIPTIONS " +
				"set alarm_description = ? " +
				"where slave_id = ? " + // hexadecimal string e.g. '01', '02', '1F', 'A0'
				"  and alarm_id = ?";

		int result = 0;

		try {

			// update the device
			result = simpleJdbcTemplate.update(sql,
					alarmDescription.getAlarmDescription(),
					slaveIdString,
					alarmIdString);

		} catch (DataAccessException e) {

			logger.error("ERROR while updating the device", e);
//			e.printStackTrace();

		} // end catch

	} // end updateAlarmDescription


	/**
	 *
	 */
	@Override
	public void deleteOldAlarms() {

		String ROWS_TO_KEEP = "100";

		// define the SQL select statement
		String selectSql = "select * " +
				"from DEVICES";

		RowMapper<Device> mapper = new RowMapper<Device>() {

			// implement the mapRow method in order to create a Device object
			public Device mapRow(ResultSet rs, int rowNum) throws SQLException {

				Device device = new Device();

				device.setSlaveId(Byte.parseByte(trim(rs.getString("SLAVE_ID"))));
				device.setStatus(trim(rs.getString("STATUS")));

				return device;

			} // end mapRow

		}; // end anonymous inner class RowMapper

		List<Device> deviceList = null;

		try {

			// retrieve the list of devices
			deviceList = simpleJdbcTemplate.query(selectSql, mapper);

		} catch (DataAccessException e) {

			logger.error("ERROR while retrieving the list of devices", e);
//			e.printStackTrace();

		} // end catch

		if (deviceList != null && deviceList.size() > 0) {

			logger.info("start to delete the old alarm records, keeping only the latest " + ROWS_TO_KEEP + " records for each device");

			for (Device device : deviceList) {

				// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
				String slaveIdString = String.format("%1$02X", device.getSlaveId());

				/**
				 * Example:
				 *
				 * delete from alarms
				 * where slave_id = '01'
				 *     and data_id not in (
				 *       select data_id from alarms
				 *           where slave_id = '01'
				 *           order by data_id desc
				 *           limit 100)
				 */

				// define the SQL delete statement
				String deleteSql = "delete from ALARMS " +
						"where slave_id = ? " + // hexadecimal string e.g. '01', '02', '1F', 'A0'
						"      and data_id not in (" +
						"          select data_id from ALARMS " +
						"              where slave_id = ? " + // hexadecimal string e.g. '01', '02', '1F', 'A0'
						"                  order by data_id desc " +
						"                  limit " + ROWS_TO_KEEP + ")";

				int result = 0;

				try {

					// delete the old alarms
					result = simpleJdbcTemplate.update(deleteSql, slaveIdString, slaveIdString);

					logger.info("deleted " + result + " old alarms for slaveId = " + slaveIdString);

				} catch (DataAccessException e) {

					logger.error("ERROR while deleting the old alarms for slaveId = " + slaveIdString, e);
//					e.printStackTrace();

				} // end catch

			} // end for device

		} // end if deviceList

	} // end deleteOldAlarms


	/**
	 * private methods
	 */

	private String trim(String s) {
		return s == null ? null : s.trim();
	}

} // end AlarmDao
