package com.dimingo.poliform.mac.commons.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.dimingo.poliform.mac.commons.bean.Command;
import com.dimingo.poliform.mac.commons.bean.Device;


/**
 * CommandDao
 *
 * @author giovanni
 *
 */
public class DeviceDao implements IDeviceDao {

    // SimpleJdbcTemplate-style...
    private SimpleJdbcTemplate simpleJdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }


    /**
     * Default constructor
     */
    public DeviceDao() {

    } // end constructor


    /**
     * findCommandBySlaveId
     */
    @Override
    public List<Device> findAllDevices() {

        // define the SQL select statement
        String sql = "select * " +
                "from DEVICES";

        RowMapper<Device> mapper = new RowMapper<Device>() {

            // implement the mapRow method in order to create a Device object
            public Device mapRow(ResultSet rs, int rowNum) throws SQLException {

                Device device = new Device();

                device.setSlaveId(Byte.parseByte(trim(rs.getString("SLAVE_ID"))));
                device.setStatus(trim(rs.getString("STATUS")));
                device.setSerialPort(trim(rs.getString("SERIAL_PORT")));
                device.setIpAddress(trim(rs.getString("IP_ADDRESS")));
                device.setIpPort(trim(rs.getString("IP_PORT")));
                device.setSlaveLocation(trim(rs.getString("SLAVE_LOCATION")));
                device.setSlaveDescription(trim(rs.getString("SLAVE_DESCRIPTION")));
                device.setName1(trim(rs.getString("NAME_1")));
                device.setPhone1(trim(rs.getString("PHONE_1")));
                device.setEmail1(trim(rs.getString("EMAIL_1")));
                device.setName2(trim(rs.getString("NAME_2")));
                device.setPhone2(trim(rs.getString("PHONE_2")));
                device.setEmail2(trim(rs.getString("EMAIL_2")));
                device.setName3(trim(rs.getString("NAME_3")));
                device.setPhone3(trim(rs.getString("PHONE_3")));
                device.setEmail3(trim(rs.getString("EMAIL_3")));
                device.setName4(trim(rs.getString("NAME_4")));
                device.setPhone4(trim(rs.getString("PHONE_4")));
                device.setEmail4(trim(rs.getString("EMAIL_4")));
                device.setName5(trim(rs.getString("NAME_5")));
                device.setPhone5(trim(rs.getString("PHONE_5")));
                device.setEmail5(trim(rs.getString("EMAIL_5")));

                return device;

            } // end mapRow

        }; // end anonymous inner class RowMapper

        List<Device> deviceList = null;

        try {

            // retrieve the list of devices
            deviceList = simpleJdbcTemplate.query(sql, mapper);

        } catch (DataAccessException e) {

            e.printStackTrace();

        } // end catch

        return deviceList;

    } // end findAllDevices


    @Override
    public Device findDeviceBySlaveId(byte slaveId) {

        // convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
        String slaveIdString = String.format("%1$02X", slaveId);

        // define the SQL select statement
        String sql = "select * " +
                "from DEVICES " +
                "where slave_id = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

        RowMapper<Device> mapper = new RowMapper<Device>() {

            // implement the mapRow method in order to create a Device object
            public Device mapRow(ResultSet rs, int rowNum) throws SQLException {

                Device device = new Device();

                device.setSlaveId(Byte.parseByte(trim(rs.getString("SLAVE_ID"))));
                device.setStatus(trim(rs.getString("STATUS")));
                device.setSerialPort(trim(rs.getString("SERIAL_PORT")));
                device.setIpAddress(trim(rs.getString("IP_ADDRESS")));
                device.setIpPort(trim(rs.getString("IP_PORT")));
                device.setSlaveLocation(trim(rs.getString("SLAVE_LOCATION")));
                device.setSlaveDescription(trim(rs.getString("SLAVE_DESCRIPTION")));
                device.setName1(trim(rs.getString("NAME_1")));
                device.setPhone1(trim(rs.getString("PHONE_1")));
                device.setEmail1(trim(rs.getString("EMAIL_1")));
                device.setName2(trim(rs.getString("NAME_2")));
                device.setPhone2(trim(rs.getString("PHONE_2")));
                device.setEmail2(trim(rs.getString("EMAIL_2")));
                device.setName3(trim(rs.getString("NAME_3")));
                device.setPhone3(trim(rs.getString("PHONE_3")));
                device.setEmail3(trim(rs.getString("EMAIL_3")));
                device.setName4(trim(rs.getString("NAME_4")));
                device.setPhone4(trim(rs.getString("PHONE_4")));
                device.setEmail4(trim(rs.getString("EMAIL_4")));
                device.setName5(trim(rs.getString("NAME_5")));
                device.setPhone5(trim(rs.getString("PHONE_5")));
                device.setEmail5(trim(rs.getString("EMAIL_5")));

                return device;

            } // end mapRow

        }; // end anonymous inner class RowMapper

        Device device = null;

        try {

            // retrieve the device
            device = simpleJdbcTemplate.query(sql, mapper, slaveIdString).get(0);

        } catch (DataAccessException e) {

            e.printStackTrace();

        } // end catch

        return device;

    } // end findDeviceBySlaveId


    @Override
    public void updateDevice(Device device) {

        // convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
        String slaveIdString = String.format("%1$02X", device.getSlaveId());

        // define the SQL update statement
        String sql = "update DEVICES " +
                "set status = ?, " +
                "serial_port = ?, " +
                "ip_address = ?, " +
                "ip_port = ?, " +
                "slave_location = ?, " +
                "slave_description = ?, " +
                "name_1 = ?, " +
                "phone_1 = ?, " +
                "email_1 = ?, " +
                "name_2 = ?, " +
                "phone_2 = ?, " +
                "email_2 = ?, " +
                "name_3 = ?, " +
                "phone_3 = ?, " +
                "email_3 = ?, " +
                "name_4 = ?, " +
                "phone_4 = ?, " +
                "email_4 = ?, " +
                "name_5 = ?, " +
                "phone_5 = ?, " +
                "email_5 = ? " +
                "where slave_id = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

        int result = 0;

        try {

            // update the device
            result = simpleJdbcTemplate.update(sql,
                    device.getStatus(),
                    device.getSerialPort(),
                    device.getIpAddress(),
                    device.getIpPort(),
                    device.getSlaveLocation(),
                    device.getSlaveDescription(),
                    device.getName1(),
                    device.getPhone1(),
                    device.getEmail1(),
                    device.getName2(),
                    device.getPhone2(),
                    device.getEmail2(),
                    device.getName3(),
                    device.getPhone3(),
                    device.getEmail3(),
                    device.getName4(),
                    device.getPhone4(),
                    device.getEmail4(),
                    device.getName5(),
                    device.getPhone5(),
                    device.getEmail5(),
                    slaveIdString);

        } catch (DataAccessException e) {

            e.printStackTrace();

        } // end catch

    } // end updateDevice


    @Override
    public void updateDeviceConf(Device device) {

        // convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
        String slaveIdString = String.format("%1$02X", device.getSlaveId());

        // define the SQL update statement
        String sql = "update DEVICES " +
                "set status = ?, " +
                "serial_port = ?, " +
                "ip_address = ?, " +
                "ip_port = ?, " +
                "slave_location = ?, " +
                "slave_description = ? " +
                "where slave_id = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

        int result = 0;

        try {

            // update the device
            result = simpleJdbcTemplate.update(sql,
                    device.getStatus(),
                    device.getSerialPort(),
                    device.getIpAddress(),
                    device.getIpPort(),
                    device.getSlaveLocation(),
                    device.getSlaveDescription(),
                    slaveIdString);

        } catch (DataAccessException e) {

            e.printStackTrace();

        } // end catch

    } // end updateDeviceConf


    @Override
    public void updateDeviceEmail(Device device) {

        // convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
        String slaveIdString = String.format("%1$02X", device.getSlaveId());

        // define the SQL update statement
        String sql = "update DEVICES " +
                "set name_1 = ?, " +
                "phone_1 = ?, " +
                "email_1 = ?, " +
                "name_2 = ?, " +
                "phone_2 = ?, " +
                "email_2 = ?, " +
                "name_3 = ?, " +
                "phone_3 = ?, " +
                "email_3 = ?, " +
                "name_4 = ?, " +
                "phone_4 = ?, " +
                "email_4 = ?, " +
                "name_5 = ?, " +
                "phone_5 = ?, " +
                "email_5 = ? " +
                "where slave_id = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

        int result = 0;

        try {

            // update the device
            result = simpleJdbcTemplate.update(sql,
                    device.getName1(),
                    device.getPhone1(),
                    device.getEmail1(),
                    device.getName2(),
                    device.getPhone2(),
                    device.getEmail2(),
                    device.getName3(),
                    device.getPhone3(),
                    device.getEmail3(),
                    device.getName4(),
                    device.getPhone4(),
                    device.getEmail4(),
                    device.getName5(),
                    device.getPhone5(),
                    device.getEmail5(),
                    slaveIdString);

        } catch (DataAccessException e) {

            e.printStackTrace();

        } // end catch

    } // end updateDeviceEmail


    /**
     * private methods
     */

    private String trim(String s) {
        return s == null ? null : s.trim();
    }

} // end DeviceDao
