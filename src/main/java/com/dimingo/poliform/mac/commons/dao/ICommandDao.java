package com.dimingo.poliform.mac.commons.dao;

import com.dimingo.poliform.mac.commons.bean.Command;


public interface ICommandDao {
	
	Command findCommandBySlaveId(byte slaveId);
	
	void updateCommand(Command command);

	void updateReset(byte slaveId, Boolean reset);

	void updateMute(byte slaveId, Boolean mute);

} // end interface
