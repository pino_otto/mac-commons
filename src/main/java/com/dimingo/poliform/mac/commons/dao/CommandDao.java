package com.dimingo.poliform.mac.commons.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.dimingo.poliform.mac.commons.bean.Command;


/**
 * CommandDao
 * 
 * @author giovanni
 *
 */
public class CommandDao implements ICommandDao {

	// SimpleJdbcTemplate-style...
	private SimpleJdbcTemplate simpleJdbcTemplate;

	public void setDataSource(DataSource dataSource) {
	    this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}
	
	
	/**
	 * Default constructor
	 */
	public CommandDao() {
		
	} // end constructor
	
	
	/**
	 * findCommandBySlaveId
	 */
	@Override
	public Command findCommandBySlaveId(byte slaveId) {
		
		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);
		
		// define the SQL select statement
		String sql = "select * " +
					   "from COMMANDS " +
				 	   "where SLAVE_ID = ?";  // hexadecimal string e.g. '01', '02', '1F', 'A0'
		
		RowMapper<Command> mapper = new RowMapper<Command>() {
		    
	        // implement the mapRow method in order to create a Command object
	        public Command mapRow(ResultSet rs, int rowNum) throws SQLException {
	            
	        	Command command = new Command();
	        	
	            command.setSlaveId(Byte.parseByte(rs.getString("SLAVE_ID").trim()));
	            
	            command.setReset(rs.getBoolean("RESET"));
	            command.setMute(rs.getBoolean("MUTE"));
	            	            
	            return command;
	        	
	        } // end mapRow
	        
	    }; // end anonymous inner class RowMapper
	    
	    Command command = null;
	    
	    try {
	    	
			// retrieve the command
			command = simpleJdbcTemplate.queryForObject(sql, mapper, slaveIdString);
			
		} catch (DataAccessException e) {
			
			e.printStackTrace();
			
		} // end catch

	    return command;
		
	} // end findCommandBySlaveId
	

	@Override
	public void updateCommand(Command command) {
		
		// define the SQL update statement
		String sql = "update COMMANDS " +
					    "set RESET = ?, " + 
						"    MUTE = ? " +
					  "where SLAVE_ID = ?"; // end sql string
		
		//call the update method for executing the update into the db
		simpleJdbcTemplate.update(sql, 
				
				command.isReset() ? "1" : "0",
				command.isMute() ? "1" : "0",
				String.format("%1$02X", command.getSlaveId())

				); // end update call

	} // end updateCommandBySlaveId


	@Override
	public void updateReset(byte slaveId, Boolean reset) {

		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);

		// define the SQL update statement
		String sql = "update COMMANDS " +
				"set RESET = ? " +
				"where SLAVE_ID = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

		//call the update method for executing the update into the db
		simpleJdbcTemplate.update(sql,

				reset ? "1" : "0",
				slaveIdString

		); // end update call

	} // end updateReset


	@Override
	public void updateMute(byte slaveId, Boolean mute) {

		// convert the slaveId byte into an hexadecimal string with two-chars, zero-filled
		String slaveIdString = String.format("%1$02X", slaveId);

		// define the SQL update statement
		String sql = "update COMMANDS " +
				"set MUTE = ? " +
				"where SLAVE_ID = ?"; // hexadecimal string e.g. '01', '02', '1F', 'A0'

		//call the update method for executing the update into the db
		simpleJdbcTemplate.update(sql,

				mute ? "1" : "0",
				slaveIdString

		); // end update call

	} // end updateMute

}
