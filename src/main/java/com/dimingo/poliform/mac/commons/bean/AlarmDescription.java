package com.dimingo.poliform.mac.commons.bean;

public class AlarmDescription {
	
	/**
	 * Instance variables
	 */
	
	private byte slaveId;
	
	private byte alarmId;
	
	private String alarmDescription;

	
	/**
	 * Getters and Setters
	 */

	public byte getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(byte slaveId) {
		this.slaveId = slaveId;
	}

	public byte getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(byte alarmId) {
		this.alarmId = alarmId;
	}

	public String getAlarmDescription() {
		return alarmDescription;
	}

	public void setAlarmDescription(String alarmDescription) {
		this.alarmDescription = alarmDescription;
	}
	

} // end AlarmDescription
