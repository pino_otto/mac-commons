package com.dimingo.poliform.mac.commons.bean;

public class Device {

	/**
	 * Instance variables
	 */
	
	private byte slaveId;
	
	private String status;
	
	private String serialPort;
	
	private String ipAddress;
	
	private String ipPort;
	
	private String slaveLocation;
	
	private String slaveDescription;
	
	private String name1;
	
	private String phone1;
	
	private String email1;
	
	private String name2;
	
	private String phone2;
	
	private String email2;
	
	private String name3;
	
	private String phone3;
	
	private String email3;
	
	private String name4;
	
	private String phone4;
	
	private String email4;
	
	private String name5;
	
	private String phone5;
	
	private String email5;
	

	
	/**
	 * Getters and Setters
	 */

	public byte getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(byte slaveId) {
		this.slaveId = slaveId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(String serialPort) {
		this.serialPort = serialPort;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpPort() {
		return ipPort;
	}

	public void setIpPort(String ipPort) {
		this.ipPort = ipPort;
	}

	public String getSlaveLocation() {
		return slaveLocation;
	}

	public void setSlaveLocation(String slaveLocation) {
		this.slaveLocation = slaveLocation;
	}

	public String getSlaveDescription() {
		return slaveDescription;
	}

	public void setSlaveDescription(String slaveDescription) {
		this.slaveDescription = slaveDescription;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getEmail3() {
		return email3;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getPhone4() {
		return phone4;
	}

	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}

	public String getEmail4() {
		return email4;
	}

	public void setEmail4(String email4) {
		this.email4 = email4;
	}

	public String getName5() {
		return name5;
	}

	public void setName5(String name5) {
		this.name5 = name5;
	}

	public String getPhone5() {
		return phone5;
	}

	public void setPhone5(String phone5) {
		this.phone5 = phone5;
	}

	public String getEmail5() {
		return email5;
	}

	public void setEmail5(String email5) {
		this.email5 = email5;
	}
	
	
} // end Device
