package com.dimingo.poliform.mac.commons.dao;

import java.util.List;

import com.dimingo.poliform.mac.commons.bean.Alarm;
import com.dimingo.poliform.mac.commons.bean.AlarmDescription;


public interface IAlarmDao {
	
	void storeAlarm(Alarm alarm);
	
	Alarm findLastAlarmBySlaveId(byte slaveId);
	
	AlarmDescription findAlarmDescription(byte slaveId, byte alarmId);
	
	List<AlarmDescription> findAllAlarmDescriptions(byte slaveId);

	void updateAlarmDescription(AlarmDescription alarmDescription);

	void deleteOldAlarms();

} // end IAlarmDao
