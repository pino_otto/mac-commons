package com.dimingo.poliform.mac.commons.dao;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import javax.sql.DataSource;


/**
 * UserDao
 * 
 * @author giovanni
 *
 */
public class UserDao implements IUserDao {

	// SimpleJdbcTemplate-style...
	private SimpleJdbcTemplate simpleJdbcTemplate;

	public void setDataSource(DataSource dataSource) {
	    this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}


	/**
	 * Default constructor
	 */
	public UserDao() {
		
	} // end constructor
	

	@Override
	public void updateAdminEmail(String email) {
		
		// define the SQL update statement
		String sql = "update USER " +
					    "set EMAIL = ? " +
					  "where ID = 'admin'"; // end sql string
		
		//call the update method for executing the update into the db
		simpleJdbcTemplate.update(sql, email);

	} // end updateAdminEmail

}
