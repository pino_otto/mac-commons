package com.dimingo.poliform.mac.commons.dao;

import java.util.List;

import com.dimingo.poliform.mac.commons.bean.Device;

public interface IDeviceDao {
	
	List<Device> findAllDevices();
	
	Device findDeviceBySlaveId(byte slaveId);
	
	void updateDevice(Device device);

	void updateDeviceConf(Device device);

	void updateDeviceEmail(Device device);

} // end IDeviceDao
