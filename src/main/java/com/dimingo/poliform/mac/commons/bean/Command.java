package com.dimingo.poliform.mac.commons.bean;

public class Command {
	
	/**
	 * Instance variables
	 */
	
	private byte slaveId;
	
	private boolean reset;
	
	private boolean mute;
	
	
	/**
	 * Getters and Setters
	 */

	public byte getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(byte slaveId) {
		this.slaveId = slaveId;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	public boolean isMute() {
		return mute;
	}

	public void setMute(boolean mute) {
		this.mute = mute;
	}
	

} // end class
