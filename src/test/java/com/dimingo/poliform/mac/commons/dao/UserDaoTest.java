package com.dimingo.poliform.mac.commons.dao;

import junit.framework.TestCase;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserDaoTest extends TestCase {
	
	// define the user dao reference
	IUserDao userDao;


	protected void setUp() throws Exception {
		
		super.setUp();
		
		// load the spring application context
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContextTest.xml");
	    
		// get the user dao bean
		userDao = (IUserDao) applicationContext.getBean("userDao");

	} // end setUp
	

	public void testUpdateAdminEmail() {
		
		try {

			// update the admin email into the database
			userDao.updateAdminEmail("pippo.pluto@abc.com");
			
		} catch (Exception e) {
			fail("Exception while executing UserDao.updateAdminEmail('pippo.pluto@abc.com')");
		}
		
	} // end testUpdateAdminEmail


	public void testDeleteAdminEmail() {

		try {

			// delete the admin email from the database
			userDao.updateAdminEmail("");

		} catch (Exception e) {
			fail("Exception while executing UserDao.updateAdminEmail('')");
		}

	} // end testDeleteAdminEmail
	

}
