package com.dimingo.poliform.mac.commons.dao;

import java.util.List;

import junit.framework.TestCase;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dimingo.poliform.mac.commons.bean.Device;


public class DeviceDaoTest extends TestCase {
	
	// define the device dao reference
	IDeviceDao deviceDao;
	

	protected void setUp() throws Exception {
		
		super.setUp();
		
		// load the spring application context
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContextTest.xml");
	    
		// get the device dao bean
		deviceDao = (IDeviceDao) applicationContext.getBean("deviceDao");

	} // end setUp
	

	public void testFindAllDevices() {
		
		try {
			
			// find the list of devices
			List<Device> deviceList = deviceDao.findAllDevices();
			
			for (Device device : deviceList) {
				
				System.out.println("--------------------------------------");
				System.out.println("slave id = " + device.getSlaveId());
				System.out.println("status   = " + device.getStatus());
				System.out.println("serial port = " + device.getSerialPort());
				System.out.println("ip address = " + device.getIpAddress());
				System.out.println("ip port = " + device.getIpPort());
				
			} // end for
			
		} catch (Exception e) {
			fail("Exception while executing deviceDao.findAllDevices()");
		}
		
	} // end testFindAllDevices


	public void testFindDeviceBySlaveId() {

		try {

			// find the device with id 1
			Device device = deviceDao.findDeviceBySlaveId((byte) 1);

			System.out.println("--------------------------------------");
			System.out.println("slave id = " + device.getSlaveId());
			System.out.println("status   = " + device.getStatus());
			System.out.println("serial port = " + device.getSerialPort());
			System.out.println("ip address = " + device.getIpAddress());
			System.out.println("ip port = " + device.getIpPort());

		} catch (Exception e) {
			fail("Exception while executing deviceDao.findDeviceBySlaveId()");
		}

	} // end testFindDeviceBySlaveId


	public void testUpdateDevice() {

		try {

			Device device = new Device();

			device.setSlaveId((byte) 3);
			device.setStatus("0");
			device.setSerialPort("");
			device.setIpAddress("192.168.1.101");
			device.setIpPort("502");
			device.setSlaveLocation("big room");
			device.setSlaveDescription("air quality collector");
			device.setName1("name 1");
			device.setPhone1("phone 1");
			device.setEmail1("email 1");
			device.setName2("name 2");
			device.setPhone2("p 2");
			device.setEmail2("e 2");
			device.setName3("n 3");
			device.setPhone3("p 3");
			device.setEmail3("e 3");
			device.setName4("n 4");
			device.setPhone4("p 4");
			device.setEmail4("e 4");
			device.setName5("n 5");
			device.setPhone5("p 5");
			device.setEmail5("e 5");

			// update the device
			deviceDao.updateDevice(device);

			// get the device
			Device expectedDevice = deviceDao.findDeviceBySlaveId((byte) 3);

			System.out.println("--------------------------------------");
			System.out.println("slave id = " + expectedDevice.getSlaveId());
			System.out.println("status   = " + expectedDevice.getStatus());
			System.out.println("serial port = " + expectedDevice.getSerialPort());
			System.out.println("ip address = " + expectedDevice.getIpAddress());
			System.out.println("ip port = " + expectedDevice.getIpPort());

			assertEquals("192.168.1.101", expectedDevice.getIpAddress());
			assertEquals("air quality collector", expectedDevice.getSlaveDescription());
			assertEquals("e 5", expectedDevice.getEmail5());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception while executing deviceDao.updateDevice()");
		}

	} // end testUpdateDevice


	public void testUpdateDeviceConf() {

		try {

			Device device = new Device();

			device.setSlaveId((byte) 3);
			device.setStatus("0");
			device.setSerialPort("");
			device.setIpAddress("192.168.1.101");
			device.setIpPort("502");
			device.setSlaveLocation("big room");
			device.setSlaveDescription("air quality collector");

			// update the device configuration
			deviceDao.updateDeviceConf(device);

			// get the device
			Device expectedDevice = deviceDao.findDeviceBySlaveId((byte) 3);

			System.out.println("--------------------------------------");
			System.out.println("slave id = " + expectedDevice.getSlaveId());
			System.out.println("status   = " + expectedDevice.getStatus());
			System.out.println("serial port = " + expectedDevice.getSerialPort());
			System.out.println("ip address = " + expectedDevice.getIpAddress());
			System.out.println("ip port = " + expectedDevice.getIpPort());
			System.out.println("location = " + expectedDevice.getSlaveLocation());
			System.out.println("description = " + expectedDevice.getSlaveDescription());

			assertEquals("192.168.1.101", expectedDevice.getIpAddress());
			assertEquals("air quality collector", expectedDevice.getSlaveDescription());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception while executing deviceDao.updateDevice()");
		}

	} // end testUpdateDeviceConf


	public void testUpdateDeviceEmail() {

		try {

			Device device = new Device();

			device.setSlaveId((byte) 3);
			device.setName1("name 1");
			device.setPhone1("phone 1");
			device.setEmail1("email 1");
			device.setName2("name 2");
			device.setPhone2("p 2");
			device.setEmail2("e 2");
			device.setName3("n 3");
			device.setPhone3("p 3");
			device.setEmail3("e 3");
			device.setName4("n 4");
			device.setPhone4("p 4");
			device.setEmail4("e 4");
			device.setName5("n 5");
			device.setPhone5("p 5");
			device.setEmail5("e 5");

			// update the device email
			deviceDao.updateDeviceEmail(device);

			// get the device
			Device expectedDevice = deviceDao.findDeviceBySlaveId((byte) 3);

			System.out.println("--------------------------------------");
			System.out.println("slave id = " + expectedDevice.getSlaveId());
			System.out.println("status   = " + expectedDevice.getStatus());
			System.out.println("serial port = " + expectedDevice.getSerialPort());
			System.out.println("ip address = " + expectedDevice.getIpAddress());
			System.out.println("name 1 = " + expectedDevice.getName1());
			System.out.println("phone 1 = " + expectedDevice.getPhone1());
			System.out.println("email 1 = " + expectedDevice.getEmail1());

			assertEquals("n 3", expectedDevice.getName3());
			assertEquals("p 4", expectedDevice.getPhone4());
			assertEquals("email 1", expectedDevice.getEmail1());
			assertEquals("e 5", expectedDevice.getEmail5());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception while executing deviceDao.updateDeviceEmail()");
		}

	} // end testUpdateDeviceEmail
	

} // end class
