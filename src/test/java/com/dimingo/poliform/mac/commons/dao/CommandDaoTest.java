package com.dimingo.poliform.mac.commons.dao;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dimingo.poliform.mac.commons.bean.Command;

import junit.framework.TestCase;

public class CommandDaoTest extends TestCase {
	
	// define the command dao reference
	ICommandDao commandDao;
	
	// define the test slave id
	byte slaveId = 0x01;
	

	protected void setUp() throws Exception {
		
		super.setUp();
		
		// load the spring application context
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContextTest.xml");
	    
		// get the command dao bean
		commandDao = (ICommandDao) applicationContext.getBean("commandDao");

		Command command = new Command();

		// set all the attributes of command
		command.setSlaveId(slaveId);
		command.setReset(false);
		command.setMute(false);

		// update the command into the database
		commandDao.updateCommand(command);
		
	} // end setUp
	

	public void testFindCommandBySlaveId() {
		
		Command command;
		try {
			// find the command for the defined slave id
			command = commandDao.findCommandBySlaveId(slaveId);
			
			if (command != null) {
			
				assertEquals(1, command.getSlaveId());
				assertEquals(false, command.isReset());
				assertEquals(false, command.isMute());
				
			} else {
				
				System.out.println("no commands found in the database");
				
			} // end else
			
		} catch (Exception e) {
			fail("Exception while executing CommandDao.findCommandBySlaveId(" + slaveId + ")");
		}
		
	} // end testFindCommandBySlaveId
	

	public void testUpdateCommand() {
		
		// create a new Command object
		Command command = new Command();
		
		// set all the attributes of command
		command.setSlaveId((byte) 0x01);
		command.setReset(true);
		command.setMute(true);
		
		// update the command into the database
		commandDao.updateCommand(command);
		
		// define a new Command reference for reading the command from the database
		Command readCommand;
		
		readCommand = commandDao.findCommandBySlaveId(slaveId);
		
		assertEquals(0x01, readCommand.getSlaveId());
		assertEquals(true, readCommand.isReset());
		assertEquals(true, readCommand.isMute());
		
		// set all the attributes of command
		command.setSlaveId((byte) 0x02);
		command.setReset(true);
		command.setMute(false);
		
		// update the command into the database
		commandDao.updateCommand(command);
		
	} // end testUpdateCommand


	public void testUpdateReset() {

		// update the reset into the database
		commandDao.updateReset(slaveId, true);

		// define a new Command reference for reading the command from the database
		Command readCommand;

		readCommand = commandDao.findCommandBySlaveId(slaveId);

		assertEquals(0x01, readCommand.getSlaveId());
		assertEquals(true, readCommand.isReset());

	} // end testUpdateReset


	public void testUpdateMute() {

		// update the mute into the database
		commandDao.updateMute(slaveId, true);

		// define a new Command reference for reading the command from the database
		Command readCommand;

		readCommand = commandDao.findCommandBySlaveId(slaveId);

		assertEquals(0x01, readCommand.getSlaveId());
		assertEquals(true, readCommand.isMute());

	} // end testUpdateMute

}
