package com.dimingo.poliform.mac.commons.dao;

import com.dimingo.poliform.mac.commons.bean.AlarmDescription;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dimingo.poliform.mac.commons.bean.Alarm;

import junit.framework.TestCase;


public class AlarmDaoTest extends TestCase {
	
	// define the alarm dao reference
	IAlarmDao alarmDao;
	

	protected void setUp() throws Exception {
		
		super.setUp();
		
		// load the spring application context
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContextTest.xml");
	    
		// get the alarm dao bean
		alarmDao = (IAlarmDao) applicationContext.getBean("alarmDao");

	} // end setUp
	

	public void testFindLastAlarmBySlaveId() {
		
		// define the slave id
		byte slaveId = 0x01;
		
		Alarm alarm;
		try {
			// find the last alarm for the defined slave id
			alarm = alarmDao.findLastAlarmBySlaveId(slaveId);
			assertNotNull(alarm);
//			assertEquals(2, alarm.getDataId());
		} catch (Exception e) {
			fail("Exception while executing AlarmDao.testFindLastAlarmBySlaveId(" + slaveId + ")");
		}
		
	} // end testFindLastAlarm
	

	public void testStoreAlarm() {
		
		// create a new Alarm object
		Alarm alarm = new Alarm();
		
		// fill all the fields except:
		//  - the data id, which will be set by the db automatically
		//  - the timestamp, which will be set by the storeAlarm method
		alarm.setSlaveId((byte) 0x01);
		
		alarm.setGreen_01(true);
		alarm.setGreen_02(true);
		alarm.setGreen_03(true);
		alarm.setGreen_04(true);
		alarm.setGreen_05(true);
		alarm.setGreen_06(true);
		alarm.setGreen_07(true);
		alarm.setGreen_08(true);
		alarm.setGreen_09(true);
		alarm.setGreen_10(true);
		alarm.setGreen_11(true);
		alarm.setGreen_12(true);
		alarm.setGreen_13(true);
		alarm.setGreen_14(true);
		alarm.setGreen_15(true);
		alarm.setGreen_16(true);
		alarm.setGreen_17(true);
		alarm.setGreen_18(true);
		alarm.setGreen_19(true);
		alarm.setGreen_20(true);
		alarm.setGreen_21(true);
		alarm.setGreen_22(true);
		alarm.setGreen_23(true);
		alarm.setGreen_24(true);
		alarm.setGreen_25(true);
		alarm.setGreen_26(true);
		alarm.setGreen_27(true);
		alarm.setGreen_28(true);
		alarm.setGreen_29(true);
		alarm.setGreen_30(true);
		
		alarm.setRed_01(true);
		alarm.setRed_02(true);
		alarm.setRed_03(true);
		alarm.setRed_04(true);
		alarm.setRed_05(true);
		alarm.setRed_06(true);
		alarm.setRed_07(true);
		alarm.setRed_08(true);
		alarm.setRed_09(true);
		alarm.setRed_10(true);
		alarm.setRed_11(true);
		alarm.setRed_12(true);
		alarm.setRed_13(true);
		alarm.setRed_14(true);
		alarm.setRed_15(true);
		alarm.setRed_16(true);
		alarm.setRed_17(true);
		alarm.setRed_18(true);
		alarm.setRed_19(true);
		alarm.setRed_20(true);
		alarm.setRed_21(true);
		alarm.setRed_22(true);
		alarm.setRed_23(true);
		alarm.setRed_24(true);
		alarm.setRed_25(true);
		alarm.setRed_26(true);
		alarm.setRed_27(true);
		alarm.setRed_28(true);
		alarm.setRed_29(true);
		alarm.setRed_30(true);
		
		alarm.setAnom_01(true);
		alarm.setAnom_02(true);
		alarm.setAnom_03(true);
		alarm.setAnom_04(true);
		
		alarm.setReset(false);
		alarm.setMute(true);
		
		// store the alarm into the db
		alarmDao.storeAlarm(alarm);
		
	} // end testStoreAlarm


	public void testUpdateAlarmDescription() {

		// define the slave id
		byte slaveId = 0x01;

		// define the alarm id
		byte alarmId = 27;

		AlarmDescription alarmDescription = new AlarmDescription();

		alarmDescription.setSlaveId(slaveId);
		alarmDescription.setAlarmId(alarmId);
		alarmDescription.setAlarmDescription("testing");

		try {
			alarmDao.updateAlarmDescription(alarmDescription);
		} catch (Exception e) {
			fail("Exception while executing AlarmDao.updateAlarmDescription");
		}

	} // end testUpdateAlarmDescription


	public void testDeleteOldAlarms() {

		try {
			alarmDao.deleteOldAlarms();
		} catch (Exception e) {
			fail("Exception while executing AlarmDao.deleteOldAlarms");
		}

	} // end testDeleteOldAlarms

} // end class
